package hieu19.productservice.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;

public class CacheService {

    @CacheEvict(value = "product", allEntries = true)
    @Scheduled(fixedRateString = "432000000000")
    public void clearProductsCache() {
        System.out.println("Refreshing Product cache");
    }
}
