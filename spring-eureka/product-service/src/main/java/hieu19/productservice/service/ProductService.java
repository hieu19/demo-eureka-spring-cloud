package hieu19.productservice.service;

import hieu19.productservice.entity.Product;
import hieu19.productservice.repo.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CacheManager cacheManager;

    @Cacheable(value = "product", key = "'all'")
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Cacheable(value = "product", key = "#id")
    public Product getProductById(UUID id) {
        return productRepository.findById(id).orElse(null);
    }

    @CachePut(value = "product", key = "#product.id")
    public Product createOrUpdateProduct(Product product) {
        Product p = productRepository.save(product);
        cacheManager.getCache("product").evict("all");
        return p;
    }

    @CacheEvict(value = "product", key = "#id")
    public void deleteProductById(UUID id) {
        productRepository.deleteById(id);
    }
}
