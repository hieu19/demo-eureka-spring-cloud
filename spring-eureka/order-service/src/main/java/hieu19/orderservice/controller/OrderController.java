package hieu19.orderservice.controller;

import hieu19.sharedservices.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/product")
public class OrderController {
    RestTemplate restTemplate = new RestTemplate();

    @PostMapping("/create")
    public Object createProduct(@RequestBody Product product){
        log.info("start sending product to product service, payload: " + product.toString());
        Object response = restTemplate.postForEntity("http://localhost:8080/product/create", product, String.class);
        log.info("response from product service: " + response);
        return response;
    }

    @GetMapping
    public ResponseEntity getAllProducts(){
        return restTemplate.getForEntity("http://localhost:8080/product", List.class);
    }
}
