package hieu19.sharedservices.model;

import hieu19.sharedservices.model.LOV.ListOfValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductVariant {
    private Long id;

    private UUID product_id;

    private ListOfValue details;

    private BigDecimal price;

    private Integer stock;
}
